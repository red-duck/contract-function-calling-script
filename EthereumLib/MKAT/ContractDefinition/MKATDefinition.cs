using System.Numerics;
using Nethereum.ABI.FunctionEncoding.Attributes;
using Nethereum.Contracts;

namespace Ethereum.Contracts.MKAT.ContractDefinition
{
    public partial class TemporarilyFreezeSwapAndLiquifyFunction : TemporarilyFreezeSwapAndLiquifyFunctionBase { }

    [Function("temporarilyFreezeSwapAndLiquify")]
    public class TemporarilyFreezeSwapAndLiquifyFunctionBase : FunctionMessage
    {
        [Parameter("uint256", "time", 1)]
        public virtual BigInteger Time { get; set; }
    }
}
