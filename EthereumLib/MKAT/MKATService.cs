using System.Threading.Tasks;
using System.Numerics;
using Nethereum.Web3;
using Nethereum.RPC.Eth.DTOs;
using Nethereum.Contracts.ContractHandlers;
using System.Threading;
using Ethereum.Contracts.MKAT.ContractDefinition;

namespace Ethereum.Contracts.MKAT
{
    public partial class MKATService
    {
        protected Web3 Web3 { get; }

        public ContractHandler ContractHandler { get; }

        public MKATService(Web3 web3, string contractAddress)
        {
            Web3 = web3;
            ContractHandler = web3.Eth.GetContractHandler(contractAddress);
        }

        public Task<string> TemporarilyFreezeSwapAndLiquifyRequestAsync(TemporarilyFreezeSwapAndLiquifyFunction temporarilyFreezeSwapAndLiquifyFunction)
        {
            return ContractHandler.SendRequestAsync(temporarilyFreezeSwapAndLiquifyFunction);
        }

        public Task<TransactionReceipt> TemporarilyFreezeSwapAndLiquifyRequestAndWaitForReceiptAsync(TemporarilyFreezeSwapAndLiquifyFunction temporarilyFreezeSwapAndLiquifyFunction, CancellationTokenSource cancellationToken = null)
        {
            return ContractHandler.SendRequestAndWaitForReceiptAsync(temporarilyFreezeSwapAndLiquifyFunction, cancellationToken);
        }

        public Task<string> TemporarilyFreezeSwapAndLiquifyRequestAsync(BigInteger time)
        {
            var temporarilyFreezeSwapAndLiquifyFunction = new TemporarilyFreezeSwapAndLiquifyFunction();
            temporarilyFreezeSwapAndLiquifyFunction.Time = time;

            return ContractHandler.SendRequestAsync(temporarilyFreezeSwapAndLiquifyFunction);
        }

        public Task<TransactionReceipt> TemporarilyFreezeSwapAndLiquifyRequestAndWaitForReceiptAsync(BigInteger time, CancellationTokenSource cancellationToken = null)
        {
            var temporarilyFreezeSwapAndLiquifyFunction = new TemporarilyFreezeSwapAndLiquifyFunction();
            temporarilyFreezeSwapAndLiquifyFunction.Time = time;

            return ContractHandler.SendRequestAndWaitForReceiptAsync(temporarilyFreezeSwapAndLiquifyFunction, cancellationToken);
        }
    }
}
