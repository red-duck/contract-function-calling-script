﻿using System;
using System.Threading.Tasks;
using Ethereum.Contracts.MKAT;
using Ethereum.Contracts.MKAT.ContractDefinition;
using Nethereum.Web3;
using static System.Console;

namespace TestConsole
{
    class Program
    {
        static string Prompt(string msg)
        {
            WriteLine(msg);
            return ReadLine();
        }

        static async Task Main(string[] args)
        {
            string contractAddress = Prompt("Type contract address...");
            string privateKey = Prompt("Type private key...");
            string url = "https://bsc-dataseed.binance.org/";
                
            if (String.IsNullOrWhiteSpace(privateKey) || String.IsNullOrWhiteSpace(contractAddress))
                WriteLine($"Some values are wrong: PK: {privateKey}; Contract address: {contractAddress}");

            var account = new Nethereum.Web3.Accounts.Account(privateKey);
            var web3 = new Web3(account, url);

            const int freezeSeconds = 3600 * 2;
            TemporarilyFreezeSwapAndLiquifyFunction temporarilyFreeze = new() { Time = freezeSeconds };
            MKATService mKATService = new(web3, contractAddress);

            while (true)
            {
                var txId = await mKATService.TemporarilyFreezeSwapAndLiquifyRequestAsync(temporarilyFreeze);
                WriteLine("{2}: Freezed for {0} seconds, via tx: {1}", freezeSeconds, txId, DateTime.Now);
                await Task.Delay(freezeSeconds);
            }
        }
    }
}
